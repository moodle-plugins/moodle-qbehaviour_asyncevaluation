<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Adhoc task evaluating a question asynchronously.
 * @package    qbehaviour_asyncevaluation
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace qbehaviour_asyncevaluation\task;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot . '/mod/quiz/attemptlib.php');
require_once($CFG->dirroot . '/mod/quiz/accessmanager.php'); // Internally required by quiz_attempt::create_from_usage_id().

use \core\task\adhoc_task;
use \quiz_attempt;

/**
 * Adhoc task actually evaluating a question by re-processing the evaluation with the _asyncevaluation_pending flag.
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class grade_response_task extends adhoc_task {

    /**
     * {@inheritDoc}
     * @see \core\task\task_base::execute()
     */
    public function execute() {
        global $DB;

        $taskdata = $this->get_custom_data();
        $response = (array)$taskdata->response;
        $usageid = $taskdata->usageid;
        $slot = $taskdata->slot;
        $action = $taskdata->action;

        try {
            $quizattempt = quiz_attempt::create_from_usage_id($usageid);
            $questionattempt = $quizattempt->get_question_attempt($slot);
        } catch (\moodle_exception $e) {
            // The attempt may not exist anymore. It is fine, just skip.
            return;
        }

        $savedpost = $_POST;

        try {
            // Simulate POST data to let the quiz attempt handle the regrade.
            $_POST = array(
                    $questionattempt->get_behaviour_field_name($action) => 1,
                    $questionattempt->get_behaviour_field_name('_asyncevaluation_pending') => 1
            );

            foreach ($response as $field => $value) {
                $_POST[$questionattempt->get_qt_field_name($field)] = $value;
            }

            $transaction = $DB->start_delegated_transaction();

            $time = $questionattempt->get_last_step()->get_timecreated();

            // Process the action with the _asyncevaluation_pending var.
            // That will actually grade the response instead of queuing a task.
            // We call this method explicitly (instead of through $quizattempt->process_submitted_actions()),
            // so we can indicate an existing step id.
            // Not creating a new step is essential to avoid sequence check errors.
            $questionattempt->process_action(
                    $questionattempt->get_submitted_data(),
                    $time,
                    $quizattempt->get_userid(),
                    $questionattempt->get_last_step()->get_id()
            );

            // Call process_submitted_actions over nothing.
            // This will not do any action, but will commit the process_action we just called.
            $_POST = array();
            $quizattempt->process_submitted_actions($time);

            $transaction->allow_commit();

        } finally {
            $_POST = $savedpost;
        }

    }
}
