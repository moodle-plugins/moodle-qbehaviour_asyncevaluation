<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Question behaviour for the case when the student's answer is just saved until they submit the whole attempt, and then is graded.
 *
 * The grading is done asynchronously by adhoc tasks. This is useful for questions taking a long time to be evaluated.
 * @package    qbehaviour_asyncevaluation
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/../deferredfeedback/behaviour.php');

use \qbehaviour_asyncevaluation\task\grade_response_task;

/**
 * Question behaviour for deferred feedback with asynchronous evaluation.
 * @see qbehaviour_deferredfeedback
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qbehaviour_asyncevaluation extends qbehaviour_deferredfeedback {

    /**
     * {@inheritDoc}
     * @see question_behaviour::get_expected_data()
     */
    public function get_expected_data() {
        return array_merge(parent::get_expected_data(), array(
                '_asyncevaluation_pending' => PARAM_BOOL,
                'finish' => PARAM_BOOL // Also include the standard finish var so we can simulate a finish in asynchronous context.
        ));
    }

    /**
     * {@inheritDoc}
     * @see question_behaviour::get_expected_qt_data()
     */
    public function get_expected_qt_data() {
        // Override the parent method to allow qt data even when the attempt is finished and the question readonly.
        return $this->question->get_expected_data();
    }

    /**
     * {@inheritDoc}
     * @param question_attempt_pending_step $pendingstep
     * @see qbehaviour_deferredfeedback::process_finish()
     */
    public function process_finish(question_attempt_pending_step $pendingstep) {

        if ($pendingstep->has_behaviour_var('_asyncevaluation_pending')) {
            static::do_grade_response($pendingstep, $pendingstep->get_qt_data(), $this->question);
            return question_attempt::KEEP;
        }

        if ($this->qa->get_state()->is_finished()) {
            return question_attempt::DISCARD;
        }

        $response = $this->qa->get_last_step()->get_qt_data();

        if (!$this->question->is_gradable_response($response)) {
            $pendingstep->set_state(question_state::$gaveup);
        } else {
            $pendingstep->set_state(question_state::$needsgrading);
            static::queue_evaluation_task($this->qa, $response, 'finish', $pendingstep->get_user_id());
        }
        $pendingstep->set_new_response_summary($this->question->summarise_response($response));
        return question_attempt::KEEP;
    }

    /**
     * Creates and queues an adhoc task that will do the grading when executed.
     * @param question_attempt $qa
     * @param mixed $response
     * @param string $action The action that lead to queuing the task, e.g. 'submit', 'finish'...
     * @param int|null $userid
     */
    public static function queue_evaluation_task(question_attempt $qa, $response, $action, $userid = null) {
        $task = new grade_response_task();
        $taskdata = array(
                'response' => $response,
                'slot' => $qa->get_slot(),
                'action' => $action,
                'usageid' => $qa->get_usage_id()
        );
        $task->set_custom_data((object)$taskdata);
        $task->set_userid($userid);
        return \core\task\manager::queue_adhoc_task($task);
    }

    /**
     * Grades the response. For this behaviour, this will be called only in asynchronous context, from the adhoc task.
     * @param question_attempt_pending_step $pendingstep
     * @param mixed $response
     * @param question_definition $question
     */
    public static function do_grade_response(question_attempt_pending_step $pendingstep, $response, question_definition $question) {
        if (!$question->is_gradable_response($response)) {
            $pendingstep->set_state(question_state::$gaveup);
        } else {
            list($fraction, $state) = $question->grade_response($response);
            $pendingstep->set_fraction($fraction);
            $pendingstep->set_state($state);
        }
        $pendingstep->set_new_response_summary($question->summarise_response($response));
    }
}
