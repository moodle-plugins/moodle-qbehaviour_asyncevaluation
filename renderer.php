<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the renderer for the deferred feedback with asynchronous evaluation behaviour.
 * @package    qbehaviour_asyncevaluation
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Renderer for outputting parts of a question belonging to the deferred feedback with asynchronous evaluation behaviour.
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qbehaviour_asyncevaluation_renderer extends qbehaviour_renderer {

    /**
     * {@inheritDoc}
     * @param question_attempt $qa a question attempt.
     * @param question_display_options $options controls what should and should not be displayed.
     * @see qbehaviour_renderer::feedback()
     */
    public function feedback(question_attempt $qa, question_display_options $options) {
        global $DB;
        $feedback = parent::feedback($qa, $options);
        if ($qa->get_state() == question_state::$needsgrading) {
            $usageid = $qa->get_usage_id();
            $slot = $qa->get_slot();
            $tasks = $DB->get_records('task_adhoc', array('classname' => '\qbehaviour_asyncevaluation\task\grade_response_task'));
            $taskid = null;
            $running = false;
            foreach ($tasks as $task) {
                $taskdata = json_decode($task->customdata);
                if ($taskdata->usageid == $usageid && $taskdata->slot == $slot) {
                    $taskid = $task->id;
                    $running = isset($task->timestarted) ? $task->timestarted !== null : $task->nextruntime < time();
                    break;
                }
            }

            if ($taskid !== null) {
                $questiondata = array(
                        'taskid' => $taskid,
                        'usageid' => $usageid,
                        'slot' => $slot,
                        'url' => $this->page->url->out(false),
                );
                // Call js to check when this task is finished.
                $this->page->requires->string_for_js('grade', 'quiz');
                $this->page->requires->strings_for_js(array('state', 'marks'), 'question');
                $this->page->requires->string_for_js('gradehaschangedreload', 'qbehaviour_asyncevaluation');
                $divid = $qa->get_outer_question_div_unique_id();
                $this->page->requires->js_call_amd('qbehaviour_asyncevaluation/observer', 'init', array($divid, $questiondata));
                $message = get_string($running ? 'evaluating' : 'evaluatingsoon', 'qbehaviour_asyncevaluation');
                $info = html_writer::div('<i class="fa icon fa-spin fa-circle-o-notch"></i><span>' . $message . '</span>', '',
                        array('data-qbehaviour_asyncevaluation-role' => 'inprogress-message'));
            } else {
                $info = '';
            }

            return $info . $feedback;
        } else {
            return $feedback;
        }
    }
}
