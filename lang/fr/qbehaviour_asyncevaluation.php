<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qbehaviour_asyncevaluation', language 'fr'
 * @package    qbehaviour_asyncevaluation
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Évaluations asynchrones (différé)';

$string['gradehaschangedreload'] = 'La note vient peut-être de changer. Vous pouvez <a {$a->aattr}>recharger la page</a> pour voir la nouvelle note.';
$string['evaluating'] = 'Cette question est en cours d\'évaluation...';
$string['evaluatingsoon'] = 'Cette question sera bientôt évaluée...';
