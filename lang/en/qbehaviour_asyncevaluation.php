<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qbehaviour_asyncevaluation', language 'en'
 * @package    qbehaviour_asyncevaluation
 * @copyright  2023 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Asynchronous evaluations (deferred)';

$string['gradehaschangedreload'] = 'The grade may just have changed. You can <a {$a->aattr}>reload the page</a> to see the new grade.';
$string['evaluating'] = 'This question is being graded...';
$string['evaluatingsoon'] = 'This question will be graded soon...';

$string['privacy:metadata'] = 'This plugin stores no personal data.';
